from django.contrib import admin
from .models import Newsfeed,Comment

class CommentInline(admin.TabularInline): # new
    model = Comment


class NewsfeedAdmin(admin.ModelAdmin): # new
    inlines = [
        CommentInline,
    ]


admin.site.register(Newsfeed, NewsfeedAdmin)
admin.site.register(Comment)