from django.urls import path
from .views import ( NewsfeedListView,
NewsfeedDeleteView,
NewsfeedUpdateView,
NewsfeedDetailView,
NewsfeedCreateView,
test )
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.conf import settings

# newsfeed/
urlpatterns = [
    path('new/', NewsfeedCreateView.as_view(), name='newsfeed_new'), # new

    path('detail/<int:pk>/', NewsfeedDetailView.as_view(), name='newsfeed_detail'),

    path('<int:pk>/edit/',
        NewsfeedUpdateView.as_view(), name='newsfeed_edit'),
  
    path('<int:pk>/delete/',
        NewsfeedDeleteView.as_view(), name='newsfeed_delete'),
   
    path('list', NewsfeedListView.as_view(), name='newsfeed_list'),

    # path('comment', CommentView.as_view(), name='comment'),


]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
