from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied 
from django.views.generic import ListView,DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView, FormView
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy
from .models import Newsfeed, Comment
# from .forms import *
from .forms import CommentForm

class NewsfeedListView(LoginRequiredMixin, ListView):
    model = Newsfeed
    template_name = 'newsfeed/newsfeed_list.html'
    login_url = 'login'

class NewsfeedDetailView(LoginRequiredMixin, TemplateView): # new
   template_name = 'newsfeed/newsfeed_detail.html'

   def get_context_data(self, *args, **kwargs):
      context = super().get_context_data(**kwargs)
      commentform = CommentForm()
      id = kwargs['pk']
      newsfeed = Newsfeed.objects.get(id=id)
      comment = newsfeed.comment.all()
      # print(id)
      context = {'newsfeed':newsfeed , 'commentform':commentform, 'comments':comment}
      return context

   def post(self, request, pk):
      commentform = CommentForm(request.POST)
      if commentform.is_valid():
         commentfield = commentform.cleaned_data['comment']
         # newsfeedfield = commentform.cleaned_data['Newsfeed']
         save_comment = Comment(Newsfeed_id=pk, comment=commentfield, author_id=self.request.user.id)
         save_comment.save()         
         return HttpResponseRedirect('/')



class NewsfeedUpdateView(LoginRequiredMixin, UpdateView): # new
   model = Newsfeed
   fields = ('title', 'body','image',)
   template_name = 'newsfeed/newsfeed_edit.html'
   success_url = reverse_lazy('newsfeed_list')
   login_url = 'login'

   def dispatch(self, request, *args, **kwargs): # new
      obj = self.get_object()
      if obj.author != self.request.user:
         raise PermissionDenied
      return super().dispatch(request, *args, **kwargs)

class NewsfeedDeleteView(LoginRequiredMixin, DeleteView): # new
    model = Newsfeed
    template_name = 'newsfeed/newsfeed_delete.html'
    login_url = 'login'

    def dispatch(self, request, *args, **kwargs): # new
      obj = self.get_object()
      if obj.author != self.request.user:
         raise PermissionDenied
      return super().dispatch(request, *args, **kwargs)

class NewsfeedCreateView(LoginRequiredMixin, CreateView):
   model= Newsfeed
   template_name = 'newsfeed/newsfeed_new.html'
   fields = ('title', 'body','image')
   success_url = reverse_lazy('newsfeed_list')
   login_url = 'login'

   def form_valid(self, form): # new
      form.instance.author = self.request.user
      return super().form_valid(form)

class test(TemplateView): 

    template_name = "home/test.html" 


