from django.forms import ModelForm
from django import forms
from Newsfeed.models import Comment

class CommentForm(ModelForm):

    # comment = forms.CharField(widget=forms.Textarea(attrs={'rows': 40, 'cols': 40}))

    class Meta:
        model = Comment
        fields = ['comment']


