from django.db import models

# Create your models here.

class Contact(models.Model):
    name =models.CharField(max_length=255)
    email =models.CharField(max_length=100)
    ph_no =models.CharField(max_length=13)
    message=models.CharField(max_length=100)
    timestamp=models.DateTimeField(auto_now_add=True, blank=True)# this is contactus model
