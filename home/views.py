from django.shortcuts import render, HttpResponse
from django.views.generic import TemplateView, CreateView
from home.models import Contact
from home.forms import ContactForm


# class ContactView(TemplateView):
#     if request.method==['POST']:
#         name=request.POleaSTc leave it
        
#     return render(request,'contactus.html'
        
class ContactView(CreateView): 
    model = Contact
    form_class = ContactForm
    template_name = "home/contactus.html" 
    success_url = '/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed. IMPORTANT POINT yes
        # It should return an HttpResponse.
        # form.ContactForm()
        return super(ContactView, self).form_valid(form)
    
    
 
class HomeView(TemplateView):
    template_name = "home/home.html"

class AboutView(TemplateView):
    template_name = "aboutus.html"

class CeoView(TemplateView):
    template_name = "home/ceo_message.html" 

class TermsView(TemplateView):
    template_name = "terms.html"

class GalleryView(TemplateView):
    template_name = "gallery.html"

class ProductsView(TemplateView):
    template_name = "products.html"

class ProjectsView(TemplateView):
    template_name = "projects.html"

class BlogView(TemplateView):
    template_name = "blog.html"

class ClientsView(TemplateView):
    template_name = "clients.html"

class PortfolioView(TemplateView):
    template_name = "portfolio.html"

class CorporateView(TemplateView):
      template_name = "home/corporate_info.html"

class VisionView(TemplateView):
      template_name = "home/visions.html"

class LeadershipView(TemplateView):
      template_name = "home/leadership.html"




