from django.urls import path
from home.views import ContactView, HomeView, AboutView,TermsView, GalleryView,ProductsView,ClientsView,PortfolioView,ProjectsView,CeoView,CorporateView,VisionView,LeadershipView
urlpatterns = [
    path('contact/', ContactView.as_view(),name= 'contact'),
    path('',HomeView.as_view(), name= 'home'),
    path('ceo/',CeoView.as_view(),name= 'ceo_message'),
    path('about/',AboutView.as_view(), name= 'about'),
    path('terms/',TermsView.as_view(),name= 'terms'),
    path('gallery/',GalleryView.as_view(),name= 'gallery'),
    path('products/',ProductsView.as_view(),name= 'products'),
    path('projects/',ProjectsView.as_view(),name= 'projects'), 
    path('clients/',ClientsView.as_view(),name= 'clients'),
    path('portfolio/',PortfolioView.as_view(),name= 'portfolio'),
    path('corporate/',CorporateView.as_view(),name= 'corporate'),
    path('vision/',VisionView.as_view(),name= 'vision'),
    path('leadership/',LeadershipView.as_view(),name= 'leadership'),
    

]