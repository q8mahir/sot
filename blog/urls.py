from django.urls import path
from blog.views import BlogListView, BlogDetailView, BlogDetailView, BlogCreateView,BlogUpdateView

urlpatterns = [
    path('blog/<int:pk>/edit/',
         BlogUpdateView.as_view(), name='blog_edit'), 
    path('mainblog/',BlogListView.as_view(),name= 'mainblog'),
    path('blog/<int:pk>/', BlogDetailView.as_view(), name='blog_detail'),
    path('newpost/',BlogCreateView.as_view(),name= 'new_post'),
]