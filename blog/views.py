from django.shortcuts import render
from django.views.generic import ListView, DeleteView, CreateView , UpdateView
from .models import blog


# Create your views here.
class BlogListView(ListView):
    model = blog
    template_name= 'blog.html'
    context_object_name = 'all_posts_list' 

class BlogDetailView(DeleteView):
    model = blog
    template_name= 'blog_detail.html'

class BlogCreateView(CreateView):
    model = blog
    template_name = 'new_post.html'
    fields = '__all__'

class BlogUpdateView(UpdateView): # new
    model = blog
    template_name = 'blog_edit.html'
    fields = ['title', 'body']

  
# Create your views here. 




